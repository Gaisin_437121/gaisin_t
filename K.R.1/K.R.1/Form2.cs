﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace K.R._1
{
    public partial class Form2 : Form
    {
        public contEntities cont1 = new contEntities();
        public List<s_students> studentsheet;
        public Form2()
        {
            InitializeComponent();
            label4.Visible = false;
        }
        
        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
            

        }
        bool surnames = true;
        bool cod_kurss = true;
        bool cod_groups = true;
        private void button2_Click(object sender, EventArgs e)
        {
            studentsheet = (from stud in cont1.s_students
                            select stud).ToList();
            var query = (from stud in studentsheet
                         join g in cont1.s_in_group on stud.id_group equals g.id_group
                         orderby stud.id
                         select new { stud.id, stud.surname, stud.name, stud.middlename, g.kurs_num, g.group_num }).ToList();

            dataGridView1.DataSource = query;
            dataGridView1.ReadOnly = true;
            if (dataGridView1.RowCount == 0) label4.Visible = true;
            else label4.Visible = false;

            dataGridView1.Columns[0].HeaderText = "Номер зачетки";
            dataGridView1.Columns[1].HeaderText = "Фамилия";
            dataGridView1.Columns[2].HeaderText = "Имя";
            dataGridView1.Columns[3].HeaderText = "Отчество";
            dataGridView1.Columns[4].HeaderText = "Номер курса";
            dataGridView1.Columns[5].HeaderText = "Номер группы";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var query = (from stud in studentsheet
                         join g in cont1.s_in_group on stud.id_group equals g.id_group
                         orderby stud.id
                         select new { stud.id, stud.surname, stud.name, stud.middlename, g.kurs_num, g.group_num }).ToList();
            if (surnames)
            {
                dataGridView1.DataSource = query.Where(p => p.surname.ToString() == textBox1.Text.ToString()).ToList();
            }

            if (cod_groups)
            {
                dataGridView1.DataSource = query.Where(p => p.kurs_num.ToString() == textBox2.Text.ToString()).ToList();
            }

            if (cod_groups)
            {
                dataGridView1.DataSource = query.Where(p => p.group_num.ToString() == textBox3.Text.ToString()).ToList();
            }

            dataGridView1.Update();
            if (dataGridView1.RowCount == 0)
                label4.Visible = true;
            else label4.Visible = false;
        }

        
    }
}
