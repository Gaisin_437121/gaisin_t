﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Сourse_project
{
    public partial class Authorization : Form
    {
        Class1 cl1 = new Class1();
        public Authorization()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cl1.Auth(textBox1.Text,textBox2.Text,this);
            Personal_a pa = new Personal_a(this);
            Payment pay = new Payment(this);
            Call call = new Call(this);
            Chat1 chat = new Chat1(this);
            Assesm ass = new Assesm(this);
            Interface_user intuser = new Interface_user(this);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            cl1.Open_reg();
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            textBox2.PasswordChar = '\0';

        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            textBox2.PasswordChar = '*';
        }
    }
}
