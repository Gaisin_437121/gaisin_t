﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace Сourse_project
{
    class AdminClass
    {
        public homeEntities6 home = new homeEntities6();

        public void Open_payout_account()
        {
            Payout_account payac = new Payout_account();
            payac.ShowDialog();
        }
        public void Payout_account(Payout_account pa)
        {
            var query = (from us in home.userr
                         join g in home.checkk on us.id_user equals g.id_user
                         orderby g.date_check
                         select new { us.surname_us, us.name_us, us.lastname_us, us.pers_num_apart, g.date_check }).ToList();

            pa.dataGridView1.DataSource = query;
            pa.dataGridView1.ReadOnly = true;

            pa.dataGridView1.Columns[0].HeaderText = "Фамилия жильца";
            pa.dataGridView1.Columns[1].HeaderText = "Имя жильца";
            pa.dataGridView1.Columns[2].HeaderText = "Отчество жильца";
            pa.dataGridView1.Columns[3].HeaderText = "Номер лицевого счета квартиры";
            pa.dataGridView1.Columns[4].HeaderText = "Дата платежа";
        }
        public void Interface(Interface_admin intad)
        {
            var query = (from us in home.userr
                         join g in home.statement on us.id_user equals g.id_user
                         orderby g.num_state
                         select new { g.num_state, us.surname_us, us.name_us, g.comment, g.choice, g.date_st_start, g.date_st_end }).ToList();

                intad.dataGridView1.DataSource = query.Where(w => w.date_st_end == null).ToList();
                intad.dataGridView1.ReadOnly = true;
                if (intad.dataGridView1.RowCount == 0) 
                intad.label1.Visible = true;
                 else intad.label1.Visible = false;

                intad.dataGridView1.Columns[1].HeaderText = "Фамилия жильца";
                intad.dataGridView1.Columns[2].HeaderText = "Имя жильца";
                intad.dataGridView1.Columns[3].HeaderText = "Комментарий к заявке";
                intad.dataGridView1.Columns[4].HeaderText = "Требуемые сотрудники";
                intad.dataGridView1.Columns[5].HeaderText = "Дата отправления заявки";
                intad.dataGridView1.Columns[0].Visible = false;
                intad.dataGridView1.Columns[6].Visible = false;
        }
        public void Find(Payout_account pa)
        {
            var query = (from us in home.userr
                         join g in home.checkk on us.id_user equals g.id_user
                         select new { us.surname_us, us.name_us, us.lastname_us, us.pers_num_apart, g.date_check }).ToList();
            if (pa.textBox1.Text != "")
            {
                switch (pa.comboBox1.SelectedIndex)
                {
                    case 0:
                        pa.dataGridView1.DataSource = query.Where(p => p.surname_us.ToString() == pa.textBox1.Text.ToString()).ToList();
                        break;
                    case 1:
                        pa.dataGridView1.DataSource = query.Where(p => p.name_us.ToString() == pa.textBox1.Text.ToString()).ToList();
                        break;
                    case 2:
                        pa.dataGridView1.DataSource = query.Where(p => Notnull(p.lastname_us).ToString() == pa.textBox1.Text.ToString()).ToList();
                        break;
                    case 3:
                        pa.dataGridView1.DataSource = query.Where(p => p.pers_num_apart.ToString() == pa.textBox1.Text.ToString()).ToList();
                        break;
                }
            }
            else
                pa.dataGridView1.DataSource = query;

            pa.dataGridView1.Update();
        }
        public void Open_app_account()
        {
            Application_account apac = new Application_account();
            apac.ShowDialog();
        }
        public void App_account_new(Application_account apc)
        {
            var query = (from us in home.userr
                         join g in home.statement on us.id_user equals g.id_user
                         orderby g.num_state
                         select new { g.num_state, us.surname_us, us.name_us, g.comment, g.choice, g.date_st_start, g.date_st_end }).ToList();

            apc.dataGridView1.DataSource = query.Where(w => w.date_st_end == null).ToList();
            apc.dataGridView1.ReadOnly = true;
            if (apc.dataGridView1.RowCount == 0) 
                apc.label2.Visible = true;
            else apc.label2.Visible = false;

            apc.dataGridView1.Columns[0].HeaderText = "Номер заявления";
            apc.dataGridView1.Columns[1].HeaderText = "Фамилия жильца";
            apc.dataGridView1.Columns[2].HeaderText = "Имя жильца";
            apc.dataGridView1.Columns[3].HeaderText = "Комментарий к заявке";
            apc.dataGridView1.Columns[4].HeaderText = "Требуемые сотрудники";
            apc.dataGridView1.Columns[5].HeaderText = "Дата отправления заявки";
            apc.dataGridView1.Columns[6].Visible = false;
        }
        public void App_account_notcheck(Application_account apc)
        {
            var query = (from us in home.userr
                         join g in home.statement on us.id_user equals g.id_user
                         join b in home.employee on g.id_user equals b.id_emp
                         orderby g.date_st_start
                         select new { us.surname_us, us.name_us, g.comment, g.choice, g.user_rev, b.surname_emp, b.name_emp, b.post, g.date_st_start, g.date_st_end, }).ToList();

            apc.dataGridView2.DataSource = query;
        }
        public void App_account_comp(Application_account apc)
        {
            var query = (from us in home.userr
                         join g in home.statement on us.id_user equals g.id_user
                         join b in home.employee on g.id_user equals b.id_emp
                         orderby g.date_st_end
                         select new { us.surname_us, us.name_us, g.comment, g.choice, g.user_rev, b.surname_emp, b.name_emp, b.post, g.date_st_start, g.date_st_end, }).ToList();

            apc.dataGridView2.DataSource = query.Where(w => w.date_st_end != null).ToList();
            apc.dataGridView2.ReadOnly = true;

            apc.dataGridView2.Columns[0].HeaderText = "Фамилия жильца";
            apc.dataGridView2.Columns[1].HeaderText = "Имя жильца";
            apc.dataGridView2.Columns[2].HeaderText = "Комментарий к заявке";
            apc.dataGridView2.Columns[3].HeaderText = "Требуемые сотрудники";
            apc.dataGridView2.Columns[4].HeaderText = "Отзыв о проделанной работе сотрудника";
            apc.dataGridView2.Columns[5].HeaderText = "Фамилия сотрудника";
            apc.dataGridView2.Columns[6].HeaderText = "Имя сотрудника";
            apc.dataGridView2.Columns[7].HeaderText = "Должность сотрудника";
            apc.dataGridView2.Columns[8].HeaderText = "Дата отправления заявки";
            apc.dataGridView2.Columns[9].HeaderText = "Дата завершения заявки ";
        }
        public void App_account_check(Application_account apc)
        {
            var query = (from us in home.userr
                         join g in home.statement on us.id_user equals g.id_user
                         join b in home.employee on g.id_user equals b.id_emp
                         orderby g.date_st_end
                         select new { us.surname_us, us.name_us, g.comment, g.choice, g.user_rev, b.surname_emp, b.name_emp, b.post, g.date_st_start, g.date_st_end, }).ToList();
            apc.dataGridView2.DataSource = query.Where(p => Notnull(p.user_rev).ToString() != "null").Where(w => w.date_st_end != null).ToList();
            apc.dataGridView2.ReadOnly = true;

            apc.dataGridView2.Columns[0].HeaderText = "Фамилия жильца";
            apc.dataGridView2.Columns[1].HeaderText = "Имя жильца";
            apc.dataGridView2.Columns[2].HeaderText = "Комментарий к заявке";
            apc.dataGridView2.Columns[3].HeaderText = "Требуемые сотрудники";
            apc.dataGridView2.Columns[4].HeaderText = "Отзыв о проделанной работе сотрудника";
            apc.dataGridView2.Columns[5].HeaderText = "Фамилия сотрудника";
            apc.dataGridView2.Columns[6].HeaderText = "Имя сотрудника";
            apc.dataGridView2.Columns[7].HeaderText = "Должность сотрудника";
            apc.dataGridView2.Columns[8].HeaderText = "Дата отправления заявки";
            apc.dataGridView2.Columns[9].HeaderText = "Дата завершения заявки ";
        }
        public void Confirm(Application_account apc)
        {
            List<employee> query1 = (from emp in home.employee
                                     select emp).ToList();

            CheckBox[] chbox = new CheckBox[] { apc.checkBox1, apc.checkBox2, apc.checkBox3, apc.checkBox4 };
            int id = home.employee.Max(n => n.id_emp);
            List<string> checklist = new List<string>();
            for (int i = 0; i < chbox.Length; i++)
            {
                if (chbox[0].Checked == true && chbox[1].Checked == true && chbox[2].Checked == true && chbox[3].Checked == true)
                {
                    foreach (var item in query1.OrderBy(o => o.post))
                    {
                        apc.checkedListBox1.Items.Clear();
                        checklist.Add(item.surname_emp + " " + item.name_emp);
                    }
                }
                else if (chbox[i].Checked == true)
                {
                    foreach (var item in query1.Where(o => o.post == chbox[i].Text))
                    {
                        checklist.Add(item.surname_emp + " " + item.name_emp);
                    }
                }
                else if (chbox[i].Checked == false)
                {
                    apc.checkedListBox1.Items.Clear();
                }
            }
            List<string> newlist = checklist.Distinct().ToList();
            foreach (string s in newlist)
                apc.checkedListBox1.Items.Add(s);
        }
        public void Confirm_button(Application_account apc)
        {
            List<statement> query = (from st in home.statement
                                     select st).ToList();
            List<employee> query1 = (from emp in home.employee
                                     select emp).ToList();
            int id = 0;
            foreach (string s in apc.checkedListBox1.CheckedItems)
            {
                var itemm = query1.First(a => a.surname_emp == Name_surname(s, 1) && a.name_emp == Name_surname(s, 2));
                id = itemm.id_emp;
            }
            try
            {
                if (apc.dataGridView1.SelectedCells.Count == 1)
                {
                    statement item = query.First(w => w.num_state.ToString() == apc.dataGridView1.SelectedCells[0].
                    OwningRow.Cells[0].Value.ToString());
                    item.date_st_end = DateTime.Today;
                    item.id_emp = id;
                    home.SaveChanges();
                    MessageBox.Show("Заявка потверждена!");
                }
                else
                {
                    MessageBox.Show("На данный момент нет новых заявок", "Внимание");
                    Application.OpenForms[0].Focus();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Вы не выбрали ни одного сотрудника", "Ошибка");
            }
            CheckBox[] chbox = new CheckBox[] { apc.checkBox1, apc.checkBox2, apc.checkBox3, apc.checkBox4 };
            for (int i = 0; i < chbox.Length; i++)
            {
                chbox[i].Checked = false;
            }
            apc.checkedListBox1.Items.Clear();
        }

        public void Open_emp_acc()
        {
            Employee_account empac = new Employee_account();
            empac.ShowDialog();
        }
        public List<employee> empsheet;
        public void Emp_acc(Employee_account empac)
        {
            empsheet = (from emp in home.employee
                            select emp).ToList();
            var query = (from emp in empsheet
                         orderby emp.id_emp
                         select emp).ToList();

            empac.dataGridView1.DataSource = query;
            empac.dataGridView1.ReadOnly = true;

            empac.dataGridView1.Columns[0].HeaderText = "Номер сотрудника";
            empac.dataGridView1.Columns[1].HeaderText = "Фамилия сотрудника";
            empac.dataGridView1.Columns[2].HeaderText = "Имя сотрудника";
            empac.dataGridView1.Columns[3].HeaderText = "Отчество сотрудника";
            empac.dataGridView1.Columns[4].HeaderText = "Номер телефона";
            empac.dataGridView1.Columns[5].HeaderText = "Должность";
            empac.dataGridView1.Columns[6].Visible = false;
            empac.dataGridView1.Update();
        }
        public void Emp_acc_find(Employee_account empac)
        {
            var query = (from emp in home.employee
                         orderby emp.id_emp
                         select emp).ToList();
            if (empac.textBox1.Text != "")
            {
                switch (empac.comboBox1.SelectedIndex)
                {
                    case 0:
                        empac.dataGridView1.DataSource = query.Where(p => p.surname_emp.ToString() == FirstUpper(empac.textBox1.Text)).ToList();
                        break;
                    case 1:
                        empac.dataGridView1.DataSource = query.Where(p => p.name_emp.ToString() == FirstUpper(empac.textBox1.Text)).ToList();
                        break;
                    case 2:
                        empac.dataGridView1.DataSource = query.Where(p => p.lastname_emp.ToString() == FirstUpper(empac.textBox1.Text)).ToList();
                        break;
                    case 3:
                        empac.dataGridView1.DataSource = query.Where(p => p.ph_num.ToString() == FirstUpper(empac.textBox1.Text.ToString())).ToList();
                        break;
                    case 4:
                        empac.dataGridView1.DataSource = query.Where(p => p.post.ToString() == FirstUpper(empac.textBox1.Text.ToString())).ToList();
                        break;
                }
            }
            else
                empac.dataGridView1.DataSource = query;

            empac.dataGridView1.Update();

        }
        public void Open_emp_add()
        {
            Add_emp ad = new Add_emp();
            ad.ShowDialog();
        }
        public void Emp_add(Add_emp ad)
        {
            var query1 = (from emp in home.employee
                          select emp);
            if (query1.Any(a => a.ph_num.ToString() == ad.textBox3.Text))
            {
                MessageBox.Show("Данный номер телeфона уже был использован другим сотрудником, повторите попытку");
                ad.textBox3.Clear();
            }
            else
            {
                var query = (from emp in home.employee
                             where emp.post == ad.comboBox1.SelectedItem.ToString()
                             select emp.post).ToList();
                int id_e = home.employee.Max(n => n.id_emp) + 1;
                try
                {
                    employee new_emp = new employee
                    {
                        id_emp = id_e,
                        surname_emp = FirstUpper(ad.textBox1.Text),
                        name_emp = FirstUpper(ad.textBox2.Text),
                        lastname_emp = FirstUpper(ad.textBox4.Text),
                        ph_num = long.Parse(ad.textBox3.Text),
                        post = query[0]
                    };
                    home.employee.Add(new_emp);

                    home.SaveChanges();
                    MessageBox.Show("Новый сотрудник успешно добавлен", "Сохранение");
                    ad.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("Не все поля заполнены!", "Ошибка", MessageBoxButtons.OK);
                }
            }
        }
        public void Open_emp_edit(Employee_account empac)
        {
            List<employee> query = (from emp in home.employee
                                    select emp).ToList();
            if (empac.dataGridView1.SelectedCells.Count == 1)
            {
                employee item = query.First(w => w.id_emp.ToString() == empac.dataGridView1.SelectedCells[0].
                OwningRow.Cells[0].Value.ToString());
                Edit_emp ed = new Edit_emp(item);
                ed.ShowDialog();
            }
            else Application.OpenForms[1].Focus();
        }
            public void Emp_edit_date(Edit_emp ed,employee item)
        {
            var post_for_list = (from g in home.employee
                                   select g.post).Distinct();

            foreach (string it in post_for_list)
            {
                ed.comboBox1.Items.Add(it);
            }
            ed.textBox1.Text = item.surname_emp.ToString();
            ed.textBox2.Text = item.name_emp.ToString();
            ed.textBox4.Text = item.lastname_emp.ToString();
            ed.textBox3.Text = item.ph_num.ToString();

            var query = (from g in home.employee
                         select g.post).ToList();

            ed.comboBox1.SelectedItem = query[0];
        }
        public void Emp_edit(Edit_emp ed, employee item)
        {
            try
            {
                var result = home.employee.SingleOrDefault(w => w.id_emp == item.id_emp);
                result.surname_emp = FirstUpper(ed.textBox1.Text);
                result.name_emp = FirstUpper(ed.textBox2.Text);
                result.lastname_emp = FirstUpper(ed.textBox4.Text);
                result.ph_num = long.Parse(ed.textBox3.Text);

                var query = (from emp in home.employee
                             where emp.post == ed.comboBox1.SelectedItem.ToString()
                             select emp.post).ToList();
                result.post = query[0];
                home.SaveChanges();
                MessageBox.Show("Данные успешно отредактированы!", "Сохранение");
                ed.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Не все поля заполнены!", "Ошибка", MessageBoxButtons.OK);
            }
        }
        public string Notnull(string s)
        {
            if (s == null)
                return s = "null";
            else return s;
        }
        public string Name_surname(string s,int i)
        {
            int pos = s.LastIndexOf(' ');
            string ss = s.Substring(0, pos);
            string sss = s.Substring(pos);
            if (i == 1)
            {;
                return ss;
            }
            else if (i == 2)
            {
                return sss;
            }
            else return s;
        }
        public string FirstUpper(string s)
        {
            s = s.ToLower();
            TextInfo ti = CultureInfo.CurrentCulture.TextInfo;
                return ti.ToTitleCase(s);
        }
    }
}

