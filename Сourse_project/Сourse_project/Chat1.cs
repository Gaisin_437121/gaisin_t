﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Сourse_project
{
    public partial class Chat1 : Form
    {
        UserrClass uscl = new UserrClass();
        public static string log;
        public Chat1()
        {
            InitializeComponent();
            uscl.Chat(this);
        }
        public Chat1(Authorization auth)
        {
            InitializeComponent();
            log = auth.textBox1.Text;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox2.Text == "")
            {
                listBox1.Items.Clear();
                uscl.Chat(this);
            }
            else
            {
                uscl.Write(this, log);
                listBox1.Items.Clear();
                uscl.Chat(this);
            }
        }
        private void textBox2_MouseLeave(object sender, EventArgs e)
        {
            if (textBox2.Text == "")
                label4.Visible = true;
        }

        private void textBox2_Click(object sender, EventArgs e)
        {
            label4.Visible = false;
        }

        private void label4_MouseMove(object sender, MouseEventArgs e)
        {
            label4.Visible = false;
        }
    }
}
