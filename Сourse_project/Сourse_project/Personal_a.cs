﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Runtime.Remoting.Messaging;

namespace Сourse_project
{
    public partial class Personal_a : Form
    {
        UserrClass usclas = new UserrClass();
        public static string log;
        Class1 cl = new Class1();
        public static bool flag = false;

        public Personal_a(Authorization auth)
        {
            InitializeComponent();
            log = auth.textBox1.Text;
        }
        public Personal_a()
        {
            InitializeComponent();
            usclas.Data(this,log);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (flag == true)
            {
                usclas.Edit(this, log);
            }
            this.Close();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsLetter(ch) && ch != 8)
            {
                e.Handled = true;
            }

        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsLetter(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsLetter(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.ReadOnly = false;
            textBox2.ReadOnly = false;
            textBox4.ReadOnly = false;
            maskedTextBox1.ReadOnly = false;
            maskedTextBox1.Mask = "0000-0000-0000";
            flag = true;
        }

        private void label9_MouseMove(object sender, MouseEventArgs e)
        {
            label11.Visible = true;
        }

        private void label8_MouseMove(object sender, MouseEventArgs e)
        {
            label10.Visible = true;
        }

        private void label9_MouseLeave(object sender, EventArgs e)
        {
            label11.Visible = false;
        }

        private void label8_MouseLeave(object sender, EventArgs e)
        {
            label10.Visible = false;
        }
    }
}
