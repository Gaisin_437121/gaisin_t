﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Сourse_project
{
    public partial class Employee_account : Form
    {
        AdminClass ac = new AdminClass();
        homeEntities6 home = new homeEntities6();
        public Employee_account()
        {
            InitializeComponent();
            ac.Emp_acc(this);
        }

        private void textBox1_MouseLeave(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
                label4.Visible = true;
        }

        private void label4_MouseMove(object sender, MouseEventArgs e)
        {
            label4.Visible = false;
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            label4.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ac.Emp_acc_find(this);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            ac.Open_emp_add();
            ac.Open_emp_acc();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            ac.Open_emp_edit(this);
            ac.Open_emp_acc();
        }
    }
}
