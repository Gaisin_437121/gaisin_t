﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Сourse_project
{
    class Class1
    {
        UserrClass uscl = new UserrClass();
        public homeEntities6 home = new homeEntities6();
        public void Open_reg()
        {

            Registration reg = new Registration();
            reg.ShowDialog();
        }

        public void Reg(Registration reg)
        {
            var query = (from user in home.userr
                         select user);
            if (query.Any(a => a.login == reg.textBox1.Text))
            {
                MessageBox.Show("Данный логин уже был использован другим пользователем, повторите попытку");
                reg.textBox1.Text = "";
            }
            else
            { 
                if (reg.textBox1.TextLength > 3 && reg.textBox2.TextLength > 3)
                {
                    int id_us = home.userr.Max(n => n.id_user) + 1;

                   userr new_user = new userr
                    {
                        id_user = id_us,
                        login = reg.textBox1.Text,
                        password = reg.textBox2.Text,
                    };
                    home.userr.Add(new_user);

                    home.SaveChanges();
                    MessageBox.Show("Вы успешно зарегистрировались!","Регистрация", MessageBoxButtons.OK);
                    reg.Close();
                }
                else
                {
                    MessageBox.Show("Неверный ввод данных", "Регистрация", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    reg.textBox1.Text = "";
                    reg.textBox2.Text = "";
                    reg.label6.Visible = true;
                    reg.label6.Text = "Ваш логин и пароль должен быть не менее чем \nиз 4 символов(только цифры и латинские буквы)";
                }
            }

            
        }

        public void Auth(string login, string password, Authorization auth)
        {
            var query = (from user in home.userr
                         select user);
            if (query.Any(a => a.login == login) && query.Any(a => a.password == password))
            {

                auth.Hide();
                Interface_user iu = new Interface_user();
                iu.Show();
            }
            else if (login == "admin" && password == "admin")
            {
                auth.Hide();
                Interface_admin ia = new Interface_admin();
                ia.Show();
            }
            else MessageBox.Show("Неверный логин или пароль", "Авторизация", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        public string FirstUpper(string s)
        {
            if (String.IsNullOrEmpty(s)) return s = "[null]";
            if (s.Length == 1) return s.ToUpper();
            return s.Remove(1).ToUpper() + s.Substring(1);
        }
        public decimal Cherta(string str)
        {
            str = str.Replace("-", "").Replace(" ", "0");
            decimal d = decimal.Parse(str);
            return d;
        }

    }
}
