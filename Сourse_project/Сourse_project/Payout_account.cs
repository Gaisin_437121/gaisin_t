﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Сourse_project
{
    public partial class Payout_account : Form
    {
        AdminClass ac = new AdminClass();
        public Payout_account()
        {
            InitializeComponent();
            ac.Payout_account(this);
        }

        private void textBox1_MouseLeave(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
                label4.Visible = true;
        }

        private void label4_MouseMove(object sender, MouseEventArgs e)
        {
            label4.Visible = false;
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            label4.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ac.Find(this);
        }
    }
}
