﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Сourse_project
{
    public partial class Application_account : Form
    {
        AdminClass ac = new AdminClass();
        public Application_account()
        {
            InitializeComponent();
            ac.App_account_new(this);
            ac.App_account_comp(this);
            ac.Confirm(this);
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab == tabPage2)
            {
                checkBox6.Visible = true;
                button3.Enabled = false;
            }
            else if (tabControl1.SelectedTab == tabPage1)
            {
                checkBox6.Visible = false;
                button3.Enabled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox6.Checked == true)
            { 
                ac.App_account_check(this);
            }
            else if (checkBox6.Checked == false)
                ac.App_account_comp(this);
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ac.Confirm_button(this);
            ac.App_account_new(this);
            ac.App_account_comp(this);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            ac.Confirm(this);
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            ac.Confirm(this);
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            ac.Confirm(this);
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            ac.Confirm(this);
        }

        private void checkedListBox1_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            var list = sender as CheckedListBox;
            if (e.NewValue == CheckState.Checked)
                foreach (int index in list.CheckedIndices)
                    if (index != e.Index)
                        list.SetItemChecked(index, false);

        }
    }
}
