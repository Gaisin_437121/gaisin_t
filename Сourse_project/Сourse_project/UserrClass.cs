﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;

namespace Сourse_project
{
    class UserrClass
    {
        public homeEntities6 home = new homeEntities6();
        public static Class1 cl = new Class1();
        public void Open_PersCab()
        {
            Personal_a pa = new Personal_a();
            pa.ShowDialog();

        }
        public bool Open_button(string log)
        {
            try
            {
                var query = (from st in home.statement
                             join st2 in home.userr on st.id_user equals st2.id_user
                             where st.user_rev == null
                             select new { st2.login, st.user_rev }).ToList();

                var item = query.First(w => w.login.ToString() == log);
                if (item.user_rev == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch
            {
                return true;
            }

        }
        public void Assem_info(Assesm assem,string log)
        {
            try
            {
                var query = (from st in home.statement
                             join st2 in home.userr on st.id_user equals st2.id_user
                             where st.user_rev == null
                             select new { st2.login, st.date_st_start, st.choice }).ToList();

                var item = query.First(w => w.login.ToString() == log);
                assem.label2.Text = "Сотрудник: " + item.choice + ", дата отпрвления заявки: " + item.date_st_start;
            }
            catch
            {

            }

        }
        public void Check_data_end(string log)
        {
            var query = (from st in home.statement
                         join st2 in home.userr on st.id_user equals st2.id_user
                         select new { st2.login, st.date_st_end }).ToList();
            Mess mess = new Mess(log);
            mess.ShowDialog();
        }
        public void Check_data_close(string log)
        {
            List<userr> query = (from user in home.userr
                                 select user).ToList();
            List<statement> query1 = (from st in home.statement
                                      where st.user_rev == null
                                      select st).ToList();

            userr itemm = query.First(w => w.login.ToString() == log);

            var result = query1.First(a => a.id_user == itemm.id_user);
            result.user_rev = "[null]";
            home.SaveChanges();

        }
        public void Open_assesm()
        {
            Assesm ass = new Assesm();
            ass.ShowDialog();
        }
        public void Assesm(Assesm ass,string log)
        {
            List<userr> query = (from user in home.userr
                                 select user).ToList();
            List<statement> query1 = (from st in home.statement
                                      where st.user_rev == null
                                      select st).ToList();

            userr itemm = query.First(w => w.login.ToString() == log);
            if (String.IsNullOrWhiteSpace(ass.textBox1.Text) == false)
            {
                var result = query1.First(a => a.id_user == itemm.id_user);
                result.user_rev = ass.textBox1.Text;
                home.SaveChanges();
                MessageBox.Show("Ваш отзыв отправлен!");
                ass.Close();
            }
            else MessageBox.Show("Заполните поле!", "Отправка отзыва", MessageBoxButtons.OK);
        }
        public void Open_cost_cacl()
        {
            Cost_calc c_calc = new Cost_calc();
            c_calc.ShowDialog();
        }
            public void Open_pay()
        {
            Payment pay = new Payment();
            pay.ShowDialog();
        }
        public void Open_call()
        {
            Call call = new Call();
            call.ShowDialog();
        }
        public void Call(Call call, string log)
        {
            List<userr> query = (from user in home.userr
                                 select user).ToList();
            userr item = query.First(w => w.login.ToString() == log);

            CheckBox[] chbox = new CheckBox[] { call.checkBox1, call.checkBox2, call.checkBox3, call.checkBox4 };
            string[] str = new string[] { "Сантехник", "Плотник", "Газовик", "Электрик" };
            string ch = "";
            for (int i = 0; i < chbox.Length; i++)
            {
                if (chbox[i].Checked)
                {
                    ch = str[i];
                }
            }
            if (ch != "")
            {
                int num_st = home.statement.Max(n => n.num_state) + 1;
                if (String.IsNullOrWhiteSpace(call.textBox1.Text) == true)
                {
                    call.textBox1.Text = "[null]";
                }
                statement new_state = new statement
                {
                    num_state = num_st,
                    date_st_start = DateTime.Today,
                    choice = ch,
                    comment = call.textBox1.Text,
                    id_user = item.id_user,
                };
                home.statement.Add(new_state);
                home.SaveChanges();
                MessageBox.Show("Ваша заявка отправлена!", "Заявление", MessageBoxButtons.OK);
                call.Close();
            }
            else MessageBox.Show("Вы не выбрали ни одного сотрудника!", "Заявление", MessageBoxButtons.OK, MessageBoxIcon.Error);

        }
        public void Write(Chat1 chat, string log)
        {
        
                List<userr> query = (from user in home.userr
                                     select user).ToList();
                userr item = query.First(w => w.login.ToString() == log);
                int id_chatt = home.chat.Max(n => n.id_chat) + 1;

                chat new_mes = new chat
                {
                    id_chat = id_chatt,
                    date_chat = DateTime.Now,
                    message = chat.textBox2.Text,
                    id_user = item.id_user,
                };
                home.chat.Add(new_mes);
                home.SaveChanges();
        }
        public void Chat(Chat1 chat)
        {
            var query = (from c in home.chat
                         join c2 in home.userr on c.id_user equals c2.id_user
                         select new { c.id_chat, c.date_chat, c.message, c2.id_user, c2.name_us, c2.surname_us }).ToList();
            int id = home.chat.Max(n => n.id_chat);
            foreach (var item in query.OrderBy(o => o.date_chat))
            {
                chat.listBox1.Items.Add(item.surname_us.ToString() + " " + item.name_us.ToString() + ":" + " " + '"' + item.message.ToString() + '"');
                chat.listBox1.Items.Add(item.date_chat.ToString());
            }
        }
        public void Pay1(Payment pay,string log)
        {
            List<userr> query = (from user in home.userr
                                 select user).ToList();
            userr item = query.First(w => w.login.ToString() == log);
            if (item.pers_num_apart == null || item.pers_num_apart == 0)
            {
                pay.maskedTextBox1.Visible = true;
                pay.label13.Visible = true;
                pay.label14.Visible = true;
                pay.button1.Location = new Point(140, 351);
                pay.Size = new Size(546, 527);
            }
        }
        public void Pay2(Payment pay, string log)
        {
            List<userr> query = (from user in home.userr
                                 select user).ToList();
            userr item = query.First(w => w.login.ToString() == log);

            var query1 = (from check in home.checkk
                         select check);

            if (item.pers_num_apart != null || item.pers_num_apart != 0)
            {
                    int id_ch = home.checkk.Max(n => n.id_check) + 1;

                    checkk new_checkk = new checkk
                    {
                        id_check = id_ch,
                        gas = Convert.ToDecimal(pay.textBox8.Text),
                        electricity = Convert.ToDecimal(pay.textBox9.Text),
                        hwater = Convert.ToDecimal(pay.textBox7.Text),
                        cwater = Convert.ToDecimal(pay.textBox6.Text),
                        id_user = item.id_user,
                        date_check = DateTime.Now,
                    };
                    home.checkk.Add(new_checkk);

                    home.SaveChanges();

                    Process.Start("https://www.gosuslugi.ru/10373/1");
                    MessageBox.Show("Успешно!");
                    pay.Close();
            }
            else
            {
                if (cl.Cherta(pay.maskedTextBox1.Text) == 0)
                {
                    MessageBox.Show("Номер лицевого счета квартиры не заполнен!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    try
                    {
                        var result = home.userr.SingleOrDefault(a => a.id_user == item.id_user);
                        result.pers_num_apart = cl.Cherta(pay.maskedTextBox1.Text);

                        int id_ch = home.checkk.Max(n => n.id_check) + 1;

                        checkk new_checkk = new checkk
                        {
                            id_check = id_ch,
                            gas = Convert.ToDecimal(pay.textBox8.Text),
                            electricity = Convert.ToDecimal(pay.textBox9.Text),
                            hwater = Convert.ToDecimal(pay.textBox7.Text),
                            cwater = Convert.ToDecimal(pay.textBox6.Text),
                            id_user = item.id_user,
                            date_check = DateTime.Now,
                        };
                        home.checkk.Add(new_checkk);
                        home.SaveChanges();
                        Process.Start("https://www.gosuslugi.ru/10373/1");
                        pay.Close();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Данного номера лицевого счета квартиры не существует", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
        public void Cost_cacl(Cost_calc cost)
        {
            cost.label11.Text = "рублей";
            cost.label12.Text = "рублей";
            cost.label5.Text = "рублей";
            cost.label7.Text = "рублей";
            cost.label13.Visible = true;
            cost.label15.Visible = true;
            cost.label14.Visible = true;
            cost.textBox5.Visible = true;
            cost.button1.Location = new Point(140, 351);
            cost.Size = new Size(564, 534);
            cost.textBox1.Text = (Convert.ToDouble(cost.textBox1.Text) * 4.65).ToString();
            cost.textBox2.Text = (Convert.ToDouble(cost.textBox2.Text) * 63.4).ToString();
            cost.textBox4.Text = (Convert.ToDouble(cost.textBox4.Text) * 31.58).ToString();
            cost.textBox3.Text = (Convert.ToDouble(cost.textBox3.Text) * 55.92).ToString();
            cost.textBox5.Text = (Convert.ToDouble(cost.textBox1.Text) + Convert.ToDouble(cost.textBox2.Text) + Convert.ToDouble(cost.textBox4.Text) + 
                Convert.ToDouble(cost.textBox3.Text)).ToString();
        }
        public void Open_chat()
        {
            Chat1 chat1 = new Chat1();
            chat1.ShowDialog();
        }
        public void Open_serv_call()
        {
            Service_call chat1 = new Service_call();
            chat1.ShowDialog();
        }
        public void Data(Personal_a pa, string log)
        {
            List<userr> query = (from user in home.userr
                                 select user).ToList();
            userr item = query.First(w => w.login.ToString() == log);
            pa.textBox1.Text = item.name_us.ToString();
            pa.textBox2.Text = item.surname_us.ToString();
            pa.textBox4.Text = item.lastname_us.ToString();
            pa.maskedTextBox1.Text = item.pers_num_apart.ToString();
        }
        public void Edit(Personal_a pa,string log)
        {
            List<userr> query = (from user in home.userr
                                 select user).ToList();
            userr item = query.First(w => w.login.ToString() == log);
            if (String.IsNullOrWhiteSpace(pa.textBox1.Text) == false && String.IsNullOrWhiteSpace(pa.textBox2.Text) == false)
            {
                try
                {
                    var result = home.userr.SingleOrDefault(a => a.id_user == item.id_user);
                    result.surname_us = cl.FirstUpper(pa.textBox1.Text);
                    result.name_us = cl.FirstUpper(pa.textBox2.Text);
                    result.lastname_us = cl.FirstUpper(pa.textBox4.Text);
                    result.pers_num_apart = cl.Cherta(pa.maskedTextBox1.Text);
                    home.SaveChanges();
                    MessageBox.Show("Данные успешно сохранены!", "Сохранение", MessageBoxButtons.OK);

                }
                catch (Exception)
                {
                    MessageBox.Show("Данного номера лицевого счета квартиры не существует", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else MessageBox.Show("Не все поля заполнены!", "Изменение", MessageBoxButtons.OK);
        }
    }
}