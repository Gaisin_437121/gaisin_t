﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Сourse_project
{
    public partial class Interface_user : Form
    {
        UserrClass uscl = new UserrClass();
        public static string log;
        public Interface_user(Authorization auth)
        {
            InitializeComponent();
            log = auth.textBox1.Text;

        }
        public Interface_user()
        {
            InitializeComponent();
            webBrowser1.Url = new Uri("https://kazan.mk.ru/news/");
            webBrowser1.ScriptErrorsSuppressed = true;
        }
        public void global_FormClosed(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            webBrowser1.Navigate("https://kazan.mk.ru/news/");
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            webBrowser1.GoForward();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            webBrowser1.GoBack();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            webBrowser1.Refresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            uscl.Open_PersCab();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            uscl.Open_PersCab();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            uscl.Open_serv_call();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            uscl.Open_cost_cacl();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            uscl.Open_pay();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            uscl.Open_call();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            uscl.Open_chat();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            uscl.Check_data_end(log);
            button8.Visible = false;
        }

        private void Interface_user_MouseEnter(object sender, EventArgs e)
        {
            if (uscl.Open_button(log) == false)
                button8.Visible = true;
            else if(uscl.Open_button(log) == true)
                button8.Visible = false;
        }
    }
}
