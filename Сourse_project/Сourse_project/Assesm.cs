﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Management.Instrumentation;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Сourse_project
{
    public partial class Assesm : Form
    {
        UserrClass usclas = new UserrClass();
        public static string log;
        public Assesm(Authorization auth)
        {
            InitializeComponent();
            log = auth.textBox1.Text;
        }
        public Assesm()
        {
            InitializeComponent();
            usclas.Assem_info(this, log);
        }

        private void textBox1_MouseLeave(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
                label4.Visible = true;
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            label4.Visible = false;
        }

        private void label4_MouseMove(object sender, MouseEventArgs e)
        {
            label4.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            usclas.Assesm(this, log);
        }
    }
}
