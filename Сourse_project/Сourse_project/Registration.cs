﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Сourse_project
{
    public partial class Registration : Form
    {
        public Registration()
        {
            InitializeComponent();
        }
        Class1 cl = new Class1();
        private void button1_Click(object sender, EventArgs e)
        {
            cl.Reg(this);
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if ((e.KeyChar <= 97 || e.KeyChar >= 122) && (e.KeyChar <= 47 || e.KeyChar >= 58) && (e.KeyChar <= 65 || e.KeyChar >= 90) && !Char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if ((e.KeyChar <= 97 || e.KeyChar >= 122) && (e.KeyChar <= 47 || e.KeyChar >= 58) && (e.KeyChar <= 65 || e.KeyChar >= 90) && !Char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void pictureBox2_MouseMove(object sender, MouseEventArgs e)
        {
            textBox2.PasswordChar = '\0';
        }

        private void pictureBox2_MouseLeave(object sender, EventArgs e)
        {
            textBox2.PasswordChar = '*';
        }
    }
}
