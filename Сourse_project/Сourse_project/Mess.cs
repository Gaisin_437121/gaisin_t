﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Сourse_project
{
    public partial class Mess : Form
    {
        UserrClass uscl = new UserrClass();
        Class1 cl = new Class1();
        public static string log;
        public Mess(string logg)
        {
            InitializeComponent();
            log = logg;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            uscl.Check_data_close(log);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            uscl.Open_assesm();
        }
    }
}
