﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Сourse_project
{
    public partial class Interface_admin : Form
    {
        AdminClass ac = new AdminClass();
        public Interface_admin()
        {
            InitializeComponent();
            ac.Interface(this);
        }
        public void global_FormClosed(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ac.Open_payout_account();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ac.Open_app_account();
            ac.Interface(this);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ac.Open_emp_acc();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
