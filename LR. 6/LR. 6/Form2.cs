﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LR._6
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        public Form2(Form1 a)
        {
            InitializeComponent();
            this.textBox1.Text = a.textBox1.Text;
            this.textBox2.Text = a.textBox2.Text;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            textBox2.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Class1 clas = new Class1();
            clas.Redate(textBox1.Text, textBox2.Text, this.Text);
            this.Close();
        }
    }
}
