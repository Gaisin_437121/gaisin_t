﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LR._6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Class1 cl = new Class1();
                cl.Getname(textBox1.Text, textBox2.Text);
                if (cl.сhek)
                {
                    Form2 form2 = new Form2(this);
                    form2.Show();
                    form2.Text = cl.Name;
                }
            }
            catch(Exception)
            {
                MessageBox.Show("Ошибка авторизации");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            textBox2.Clear();
        }
    }
}
