//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace K.R._2
{
    using System;
    using System.Collections.Generic;
    
    public partial class letter
    {
        public int id_let { get; set; }
        public string mess { get; set; }
        public Nullable<int> id_send { get; set; }
        public Nullable<int> id_recip { get; set; }
    
        public virtual recipient recipient { get; set; }
        public virtual sender sender { get; set; }
    }
}
