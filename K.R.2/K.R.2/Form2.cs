﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace K.R._2
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        public Form2(Form1 a)
        {
            InitializeComponent();
            if(a.radioButton1.Checked)
            this.pictureBox1.Image = a.pictureBox1.Image;
            else if(a.radioButton2.Checked)
            this.pictureBox1.Image = a.pictureBox2.Image;
            else pictureBox1.Image = a.pictureBox3.Image;
            label1.Text = a.textBox5.Text;
            textBox1.Text = a.textBox3.Text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
