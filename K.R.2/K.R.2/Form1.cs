﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace K.R._2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public letterEntities letter = new letterEntities();
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            pictureBox1.Visible = true;
            pictureBox2.Visible = false;
            pictureBox3.Visible = false;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            pictureBox2.Visible = true;
            pictureBox3.Visible = false;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            pictureBox2.Visible = false;
            pictureBox3.Visible = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var query = (from send in letter.sender
                         select send);
            var query1 = (from rec in letter.recipient
                         select rec);
            if (query.Any(a => a.name_send == textBox4.Text) && query.Any(a => a.email_send == textBox1.Text) && query1.Any(a => a.name_recip == textBox5.Text) && query1.Any(a => a.email_recip == textBox2.Text))
            {
                Form2 f2 = new Form2(this);
                f2.Show();
            }
            else
            {
                MessageBox.Show("Вы ввели неверные данные");
            }
        }
    }
}
