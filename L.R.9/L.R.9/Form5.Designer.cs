﻿namespace L.R._9
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form5));
            System.Windows.Forms.Label title_of_purchLabel;
            System.Windows.Forms.Label city_of_purchLabel;
            this.label1 = new System.Windows.Forms.Label();
            this.booksDataSet = new L.R._9.BooksDataSet();
            this.purchaserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.purchaserTableAdapter = new L.R._9.BooksDataSetTableAdapters.purchaserTableAdapter();
            this.tableAdapterManager = new L.R._9.BooksDataSetTableAdapters.TableAdapterManager();
            this.purchaserBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.purchaserBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.title_of_purchTextBox = new System.Windows.Forms.TextBox();
            this.city_of_purchTextBox = new System.Windows.Forms.TextBox();
            title_of_purchLabel = new System.Windows.Forms.Label();
            city_of_purchLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.booksDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.purchaserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.purchaserBindingNavigator)).BeginInit();
            this.purchaserBindingNavigator.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(85, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(222, 71);
            this.label1.TabIndex = 4;
            this.label1.Text = "Таблица \"Заказы\"";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // booksDataSet
            // 
            this.booksDataSet.DataSetName = "BooksDataSet";
            this.booksDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // purchaserBindingSource
            // 
            this.purchaserBindingSource.DataMember = "purchaser";
            this.purchaserBindingSource.DataSource = this.booksDataSet;
            // 
            // purchaserTableAdapter
            // 
            this.purchaserTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.authorTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.bookTableAdapter = null;
            this.tableAdapterManager.publishTableAdapter = null;
            this.tableAdapterManager.purchaserTableAdapter = this.purchaserTableAdapter;
            this.tableAdapterManager.sales_purchaserTableAdapter = null;
            this.tableAdapterManager.salesTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = L.R._9.BooksDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // purchaserBindingNavigator
            // 
            this.purchaserBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.purchaserBindingNavigator.BindingSource = this.purchaserBindingSource;
            this.purchaserBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.purchaserBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.purchaserBindingNavigator.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.purchaserBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.purchaserBindingNavigatorSaveItem});
            this.purchaserBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.purchaserBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.purchaserBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.purchaserBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.purchaserBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.purchaserBindingNavigator.Name = "purchaserBindingNavigator";
            this.purchaserBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.purchaserBindingNavigator.Size = new System.Drawing.Size(359, 31);
            this.purchaserBindingNavigator.TabIndex = 5;
            this.purchaserBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(29, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Переместить в начало";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(29, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Переместить назад";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Положение";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 27);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Текущее положение";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(55, 20);
            this.bindingNavigatorCountItem.Text = "для {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Общее число элементов";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 6);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(29, 24);
            this.bindingNavigatorMoveNextItem.Text = "Переместить вперед";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(29, 24);
            this.bindingNavigatorMoveLastItem.Text = "Переместить в конец";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 6);
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(29, 28);
            this.bindingNavigatorAddNewItem.Text = "Добавить";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(29, 24);
            this.bindingNavigatorDeleteItem.Text = "Удалить";
            // 
            // purchaserBindingNavigatorSaveItem
            // 
            this.purchaserBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.purchaserBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("purchaserBindingNavigatorSaveItem.Image")));
            this.purchaserBindingNavigatorSaveItem.Name = "purchaserBindingNavigatorSaveItem";
            this.purchaserBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 23);
            this.purchaserBindingNavigatorSaveItem.Text = "Сохранить данные";
            this.purchaserBindingNavigatorSaveItem.Click += new System.EventHandler(this.purchaserBindingNavigatorSaveItem_Click);
            // 
            // title_of_purchLabel
            // 
            title_of_purchLabel.AutoSize = true;
            title_of_purchLabel.Location = new System.Drawing.Point(87, 151);
            title_of_purchLabel.Name = "title_of_purchLabel";
            title_of_purchLabel.Size = new System.Drawing.Size(90, 17);
            title_of_purchLabel.TabIndex = 5;
            title_of_purchLabel.Text = "title of purch:";
            // 
            // title_of_purchTextBox
            // 
            this.title_of_purchTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.purchaserBindingSource, "title_of_purch", true));
            this.title_of_purchTextBox.Location = new System.Drawing.Point(183, 148);
            this.title_of_purchTextBox.Name = "title_of_purchTextBox";
            this.title_of_purchTextBox.Size = new System.Drawing.Size(165, 22);
            this.title_of_purchTextBox.TabIndex = 6;
            // 
            // city_of_purchLabel
            // 
            city_of_purchLabel.AutoSize = true;
            city_of_purchLabel.Location = new System.Drawing.Point(87, 216);
            city_of_purchLabel.Name = "city_of_purchLabel";
            city_of_purchLabel.Size = new System.Drawing.Size(89, 17);
            city_of_purchLabel.TabIndex = 6;
            city_of_purchLabel.Text = "city of purch:";
            // 
            // city_of_purchTextBox
            // 
            this.city_of_purchTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.purchaserBindingSource, "city_of_purch", true));
            this.city_of_purchTextBox.Location = new System.Drawing.Point(182, 213);
            this.city_of_purchTextBox.Name = "city_of_purchTextBox";
            this.city_of_purchTextBox.Size = new System.Drawing.Size(165, 22);
            this.city_of_purchTextBox.TabIndex = 7;
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 321);
            this.Controls.Add(city_of_purchLabel);
            this.Controls.Add(this.city_of_purchTextBox);
            this.Controls.Add(title_of_purchLabel);
            this.Controls.Add(this.title_of_purchTextBox);
            this.Controls.Add(this.purchaserBindingNavigator);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form5";
            this.Text = "Таблица \"Заказы\"";
            this.Load += new System.EventHandler(this.Form5_Load);
            ((System.ComponentModel.ISupportInitialize)(this.booksDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.purchaserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.purchaserBindingNavigator)).EndInit();
            this.purchaserBindingNavigator.ResumeLayout(false);
            this.purchaserBindingNavigator.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private BooksDataSet booksDataSet;
        private System.Windows.Forms.BindingSource purchaserBindingSource;
        private BooksDataSetTableAdapters.purchaserTableAdapter purchaserTableAdapter;
        private BooksDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator purchaserBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton purchaserBindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox title_of_purchTextBox;
        private System.Windows.Forms.TextBox city_of_purchTextBox;
    }
}