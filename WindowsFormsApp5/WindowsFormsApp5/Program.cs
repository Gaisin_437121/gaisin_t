﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace WindowsFormsApp5
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
            Thread thr1 = new Thread(() =>
            {
                for (int i = 0; i < 5; i++)
                    Console.Write("A");
            });
            Thread thr2 = new Thread(() =>
            {
                for (int i = 0; i < 5; i++)
                    Console.Write("B");
            });
            Thread thr3 = new Thread(() =>
            {
                for (int i = 0; i < 5; i++)
                    Console.Write("C");
            });
            thr1.Start();
            thr2.Start();
            thr1.Join();
            thr2.Join();
            thr3.Start();
        }
    }
}
